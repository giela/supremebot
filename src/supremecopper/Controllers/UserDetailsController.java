/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import supremecopper.Models.Tools;
import supremecopper.SupremeCopper;

/**
 * FXML Controller class
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class UserDetailsController implements Initializable {
    private Tools tools = new Tools();
    
    @FXML
    private TextField fullNameField;
    
    @FXML
    private TextField emailField;
    
    @FXML
    private TextField telephoneField;
    
    @FXML
    private TextField addressField;
    
    @FXML
    private TextField cityField;
    
    @FXML 
    private TextField countryField;
    
    @FXML
    private TextField zipField;
    
    @FXML
    private Label warningLabel;
    
    @FXML
    private Button saveButton;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
    
    /**
     * save the input as userinfo object to supremecopper
     */
    @FXML
    private void save(ActionEvent event) throws IOException{
        String fullname = fullNameField.getText();
        String email = emailField.getText();
        String telephone = telephoneField.getText();
        String address = addressField.getText();
        String city = cityField.getText();
        String zip = zipField.getText();
        String country = countryField.getText();
        
        if(fullname.isEmpty() || email.isEmpty() || telephone.isEmpty() || 
                address.isEmpty() || city.isEmpty() || zip.isEmpty() || country.isEmpty())
        {
            warningLabel.setVisible(true);
        }else{
            SupremeCopper.userInfo.setAddress(address);
            SupremeCopper.userInfo.setCity(city);
            SupremeCopper.userInfo.setCountry(country);
            SupremeCopper.userInfo.setEmail(email);
            SupremeCopper.userInfo.setFullName(fullname);
            SupremeCopper.userInfo.setZip(zip);
            SupremeCopper.userInfo.setTelephone(telephone);
            SupremeCopper.saveUserPref();
            tools.closePage(event);
        }
    }
}
