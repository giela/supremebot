/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import supremecopper.Models.CaptchaPreferences;
import supremecopper.Models.CaptchaTask;
import supremecopper.SupremeCopper;

/**
 * FXML Controller class
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */

public class CaptchaSolverController implements Initializable {
    @FXML
    Button harvestButton;
    @FXML 
    TextField siteKeyTextField;
    @FXML
    TextField apiKeyTextField;
    private CaptchaTask captchaTask;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            SupremeCopper.getCaptchaPrefs();
        } catch (IOException ex) {
            Logger.getLogger(CaptchaSolverController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(SupremeCopper.captchaPreferences.getApiKey() != null){
            apiKeyTextField.setText(SupremeCopper.captchaPreferences.getApiKey());
        }
        if(SupremeCopper.captchaPreferences.getSiteKey() != null){
            siteKeyTextField.setText(SupremeCopper.captchaPreferences.getSiteKey());
        }
    }  
    
    @FXML
    public void harvestButtonOnAction(ActionEvent event) throws IOException{
        captchaTask = new CaptchaTask(siteKeyTextField.getText(), apiKeyTextField.getText());
        SupremeCopper.captchaPreferences = new CaptchaPreferences(apiKeyTextField.getText(), siteKeyTextField.getText());
        SupremeCopper.saveCaptchaPref();
        captchaTask.execute();
        harvestButton.setText("harvesting");
    }
}
