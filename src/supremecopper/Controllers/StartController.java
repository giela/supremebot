/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import supremecopper.Models.CopTask;
import supremecopper.Models.UserPreferences;
import supremecopper.Models.Tools;
import supremecopper.SupremeCopper;
import static supremecopper.SupremeCopper.userInfo;

/**
 * FXML Controller class
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class StartController implements Initializable {
    private Tools tool = new Tools();
    private UserPreferences prefs;
    private CopTask cop;
    private boolean active = false;
    
    @FXML
    private TextArea cmdOutputArea;
    
    @FXML
    private ComboBox sizeBox;
    
    @FXML
    private ComboBox categoryBox;
    
    @FXML
    private TextField colorField;
    
    @FXML
    private TextField keywordField;
    
    @FXML
    private Button copButton;
    
    @FXML
    private Label userLabel;
    
    @FXML 
    private Label ccLabel;
    
    @FXML
    private TextField checkoutDelayField; 
    
    @FXML
    private TextField refreshDelayField; 
            
     /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            SupremeCopper.getPref();
        } catch (IOException ex) {
            Logger.getLogger(StartController.class.getName()).log(Level.SEVERE, null, ex);
        }
        sizeBox.getItems().addAll(
            "Small",
            "Medium",
            "Large",
            "XLarge"
        );
        categoryBox.getItems().addAll(
            "all",
            "jackets",
            "shirts",
            "tops_sweaters",
            "sweatshirts",
            "pants",
            "shorts",
            "hats",
            "bags",
            "t-shirts",
            "accessories",
            "skate"
        );
        tool.setTextLabel(userLabel, ccLabel);
        
        checkoutDelayField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    checkoutDelayField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        refreshDelayField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    refreshDelayField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }  
    
    @FXML
    private void inputChanged(){
        if(colorField.getText().isEmpty() || keywordField.getText().isEmpty() || sizeBox.getSelectionModel().isEmpty() || categoryBox.getSelectionModel().isEmpty() || !Tools.isInteger(checkoutDelayField.getText()) || !Tools.isInteger(refreshDelayField.getText())){
            copButton.setDisable(true);
        }else{
            copButton.setDisable(tool.setTextLabel(userLabel, ccLabel));
        }
    }
    
    @FXML
    private void executeScript() throws Exception{
        if(active){
            cop.cancel(true);
            cop.stop();
            active = false;
            copButton.setText("Cop");
        }else{
            prefs = new UserPreferences(colorField.getText(), sizeBox.getSelectionModel().getSelectedItem().toString(), keywordField.getText(), categoryBox.getSelectionModel().getSelectedItem().toString(), Integer.valueOf(checkoutDelayField.getText()),Integer.valueOf(refreshDelayField.getText()));
            cop = new CopTask(userInfo, cmdOutputArea, prefs);
            cop.execute();
            active =  true;
            copButton.setText("Stop");
            cmdOutputArea.appendText("started!\n");
        }
    }
    
    @FXML
    private void setupUserDetails(ActionEvent event) throws IOException{
        String resource = "/supremecopper/Views/userDetails.fxml";
        Stage udStage = tool.newPage(resource, event);
        tool.catchClosingWindow(udStage, userLabel, ccLabel, copButton);
    }
    
    @FXML
    private void setupCreditCardDetails(ActionEvent event) throws IOException{
        String resource = "/supremecopper/Views/creditcardDetails.fxml";
        Stage ccdStage = tool.newPage(resource, event);
        tool.catchClosingWindow(ccdStage, userLabel, ccLabel, copButton);
    }
    
    @FXML
    private void openCaptchaSolverPage(ActionEvent event)throws IOException{
        String resource = "/supremecopper/Views/captchaSolver.fxml";
        Stage captchaStage = tool.newPage(resource, event);
        tool.catchClosingWindow(captchaStage, userLabel, ccLabel, copButton);
    }
}
