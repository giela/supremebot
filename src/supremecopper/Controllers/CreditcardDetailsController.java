/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import supremecopper.Models.Creditcard;
import supremecopper.Models.Tools;
import supremecopper.SupremeCopper;
/**
 * FXML Controller class
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class CreditcardDetailsController implements Initializable {
    @FXML
    private ComboBox typeComboBox; 
    
    @FXML
    private TextField ccnField;
    
    @FXML
    private TextField monthField;
    
    @FXML 
    private TextField yearField;
    
    @FXML
    private TextField cvvField;
    
    @FXML
    private Label warningLabel;
    
    @FXML
    private Button saveButton;
    
    private Tools tool = new Tools();
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        typeComboBox.getItems().addAll(
            "Visa",
            "American Express",
            "Mastercard",
            "Solo"
        );
    }    
    @FXML
    private void enableButton(){
        saveButton.setDisable(false);
    }
    
    /**
     * save credit card and set it to the userinfo.
     * @param event 
     */
    @FXML
    private void save(ActionEvent event) throws IOException{
        String ccn = ccnField.getText();
        String month = monthField.getText();
        String yearString = yearField.getText();
        String cvv= cvvField.getText();
        String type = typeComboBox.getSelectionModel().getSelectedItem().toString();
        
        if(ccn.isEmpty() || month.length() != 2 || yearString.length() != 4 || cvv.isEmpty()){
            warningLabel.setText("Not all textfields have been filled in.");
        }else{
            int year = Integer.parseInt(yearString);
            int monthInt = Integer.parseInt(month);
            if((monthInt > 0 && monthInt < 13) && year > 2016 ){
                Creditcard creditcard = new Creditcard(type, ccn, month, year, cvv);
                SupremeCopper.userInfo.setCreditcard(creditcard);
                SupremeCopper.saveCCPref();
                tool.closePage(event);
            }else{
                warningLabel.setText("exp. month or exp. year are not valid.");
            }
        }
    }  
}
