/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import supremecopper.Models.UserInfo;
import supremecopper.Models.Creditcard;
import supremecopper.Models.CaptchaPreferences;
import supremecopper.Models.Json;


/**
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class SupremeCopper extends Application {
    public static UserInfo userInfo = new UserInfo();
    public static CaptchaPreferences captchaPreferences = new CaptchaPreferences();
    private static Gson gson = new Gson();
    
    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/supremecopper/Views/Start.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("0टƐ-Y˩ԳƧ");
        stage.setScene(scene);
        stage.show(); 
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    /**
     * save userinfo to json file
     * @throws java.io.IOException
     */
    public static void saveUserPref() throws IOException{
        Json.writeToJson(userInfo, System.getenv("APPDATA")+"\\SupremeCopper\\userPrefs.json");
    }
    
    /**
     * save captchaprefs to json file
     * @throws java.io.IOException
     */
    public static void saveCaptchaPref() throws IOException{
        Json.writeToJson(captchaPreferences, System.getenv("APPDATA")+"\\SupremeCopper\\captchaPrefs.json");
    }
    
    /**
     * save creditcard prefs to json file
     * @throws java.io.IOException
     */
    public static void saveCCPref() throws IOException{
        Json.writeToJson(userInfo.getCreditcard(), System.getenv("APPDATA")+"\\SupremeCopper\\ccPrefs.json");
    }
    
    /**
     * get captcha from json file
     * @throws java.io.FileNotFoundException
     */
    public static void getCaptchaPrefs() throws FileNotFoundException, IOException{
        String jsonString = Json.readFromJson(System.getenv("APPDATA")+"\\SupremeCopper\\captchaPrefs.json");
        captchaPreferences = gson.fromJson(jsonString, CaptchaPreferences.class);
    }
    
    /**
     * get prefs from json file
     * @throws java.io.IOException
     */
    public static void getPref() throws IOException{
        String jsonString = Json.readFromJson(System.getenv("APPDATA")+"\\SupremeCopper\\userPrefs.json");
        userInfo = gson.fromJson(jsonString, UserInfo.class);
        //get credit card prefs
        jsonString = Json.readFromJson(System.getenv("APPDATA")+"\\SupremeCopper\\ccPrefs.json");
        Creditcard creditcard = gson.fromJson(jsonString, Creditcard.class);
        if(!creditcard.isEmpty()){
            userInfo.setCreditcard(creditcard);
        }else{
            System.out.println("empty");
        }
    }
    
}
