
var fs = require('fs');
utils = require('utils');
var casper = require('casper').create();
var x = require('casper').selectXPath;
casper.userAgent('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36');

//prefs
keyWord = casper.cli.get("keyWord").toLowerCase();
category = casper.cli.get("category");
size = casper.cli.get("size").toLowerCase();
if (size !== 'xlarge') {
    size = size.charAt(0).toUpperCase() + size.slice(1);
} else {
    size = size.charAt(0).toUpperCase() + size.charAt(1).toUpperCase() + size.slice(2);
}
colorPref = casper.cli.get("color").toLowerCase();
category = casper.cli.get("category");
checkoutDelay = casper.cli.get("checkoutdelay");
refreshDelay = casper.cli.get("refreshdelay");
//billing/shipping information
fullName = casper.cli.get("fullName");
email = casper.cli.get("email");
tel = casper.cli.raw.get("tel");
address = casper.cli.get("address");
city = casper.cli.get("city");
zip = casper.cli.get("zip");
country = casper.cli.get("country");
//creditcard information commit die shit
type = casper.cli.raw.get("type");
number = casper.cli.raw.get("number");
month = casper.cli.raw.get("month");
year = casper.cli.raw.get("year");
cvv = casper.cli.raw.get("cvv");


shopLink = 'http://www.supremenewyork.com/shop/all';

if (category !== "all") {
    shopLink += '/' + category;
}

//go to supreme and get args from command line
casper.start(shopLink, function () {
    s = new Date().getTime();
    casper.waitForSelector(x('//*[@id="nav-exit"]/li[1]/a'));

});

refresh(false);

function refresh(k) {
//get all urls on shop/all page
    if (k === false) {
        casper.thenOpen(shopLink);
    }
    var newlink = casper.evaluate(getCategoryLink, keyWord, colorPref);
    if (newlink) {
        casper.echo(newlink);
        casper.thenOpen("http://www.supremenewyork.com" + newlink);
    } else {
        casper.wait(refreshDelay, function () {
            casper.echo("not found refreshing")
            refresh(false);
        });
    }


    //select size and add to cart
    casper.then(function () {
        casper.evaluate(function (size) {
            var sizeObj = document.querySelector("select#size");
            for (var i = 0, sL = sizeObj.length; i < sL; i++) {
                if (sizeObj.options[i].text == size || sizeObj.options[i].text == '34') {
                    sizeObj.selectedIndex = i;
                    break;
                }
            }
        }, size);
        //casper.capture('item.jpg');
        casper.click(x('//*[@id="add-remove-buttons"]/input'));
        casper.waitUntilVisible(x('//*[@id="cart"]'));
    });

    //casper go to checkout screen
    casper.then(function () {
        //casper.capture('test4.jpg');
        casper.click(x('//*[@id="cart"]/a[2]'));
        casper.waitForSelector(x('//*[@id="checkout_form"]'));
    });

    //fill in checkout details + click order
    casper.then(function () {
        casper.fill('form#checkout_form', {
            'order[billing_name]': fullName,
            'order[email]': email,
            'order[tel]': tel,
            'order[billing_address]': address,
            'order[billing_city]': city,
            'order[billing_zip]': zip,
            'order[billing_country]': country,
            'credit_card[type]': type,
            'credit_card[cnb]': number,
            'credit_card[month]': month,
            'credit_card[year]': year,
            'credit_card[vval]': cvv
        });
        var twoCaptchaResponse = getCaptchaResponse();
        if (twoCaptchaResponse != false) {
            casper.evaluate(setCaptchaResponse, twoCaptchaResponse);
            casper.wait(checkoutDelay, function () {
                casper.echo(casper.fetchText(x('//*[@id="total"]')));
                //casper.evaluate(submit);
                //casper.capture('copPage.jpg');
            });
        } else {
            casper.echo('no captcha solved');
        }

    });

    casper.then(function () {
        e = new Date().getTime();
        casper.echo("Time to cop : " + (e - s) + "ms", "INFO");
        casper.wait(5000, function () {
            if (casper.exists('.error')) {
                casper.echo('Did not cop. Gave the following error');
                casper.echo(casper.fetchText('.error'));
            } else if (casper.exists('#confirmation')) {
                casper.echo(casper.fetchText('#confirmation'));
            } else {
                casper.echo('copped');
            }
        });
    });

    casper.then(function () {
        casper.exit();
    });
}

casper.run();


function setCaptchaResponse(twoCaptchaResponse) {
    document.getElementsByClassName("icheckbox_minimal")[1].click();
    document.getElementById("g-recaptcha-response").innerHTML = twoCaptchaResponse;
}
function getCaptchaResponse() {
    var captchaFile = fs.read('captcharesponses.json');
    var jsonObjArray = JSON.parse(captchaFile);
    var returnValue = false;
    for (var i = 0; i < jsonObjArray.length; i++) {
        var item = jsonObjArray[i];
        var currentTime = Math.round(new Date().getTime() / 1000.0);
        if (item.expiretime > currentTime) {
            returnValue = item.responseToken;
            break;
        }
    }
    return returnValue;
}

function getLinks() {
    var links = document.querySelectorAll('article a');
    return Array.prototype.map.call(links, function (e) {
        return e.getAttribute('href');
    });
}

function getCategoryLink(prefkeyWord, prefColor) {
    var itemName = document.querySelectorAll('article div.inner-article h1 a.name-link');
    var color = document.querySelectorAll('article div.inner-article p a.name-link');
    var g;
    for (var i = 0; i < itemName.length; i++) {
        var nameString = itemName[i].textContent.toLowerCase();
        if (nameString.indexOf(prefkeyWord) > -1) {
            var colorString = color[i].textContent.toLowerCase();
            if (colorString.indexOf(prefColor) > -1) {
                g = itemName[i].getAttribute('href');
                break;
            }
        }
    }
    return g;
}

function submit() {
    document.getElementById('checkout_form').submit();
}