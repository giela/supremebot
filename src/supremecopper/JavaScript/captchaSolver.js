var fs = require('fs');

var utils = require('utils');
var casper = require('casper').create();
var x = require('casper').selectXPath;
var jsonFile = 'captcharesponses.json';

if (!fs.exists(jsonFile)) {
    fs.touch(jsonFile);
}
var linkFile = fs.read(jsonFile);

apikey = casper.cli.get("apikey");
sitekey = casper.cli.get("sitekey");
twoCaptchaResponse = 0;
var postlink = "http://2captcha.com/in.php?key=" + apikey + "&method=userrecaptcha&googlekey=" + sitekey + "&pageurl=https://www.supremenewyork.com/checkout&json=1&proxy=91.218.127.113:8080&proxytype=HTTP";
//go to supreme and get args from command line
casper.start(postlink, function () {
    casper.waitForSelector(x('/html/body'));
});

casper.then(function () {
    var requestjson = JSON.parse(this.getPageContent());
    var captchaId = requestjson.request;
    getGCAPTCHAToken(captchaId);
});


function getGCAPTCHAToken(captchaId) {
    casper.thenOpen("http://2captcha.com/res.php?key=" + apikey + "&action=get&id=" + captchaId + "&json=1", function () {
        var solvedjson = JSON.parse(this.getPageContent());
        var solvedstatus = solvedjson.status;
        if (solvedstatus == 1) {
            casper.echo("Got captchatoke back from 2cpatcha");
            var expiredate = Math.round(new Date((new Date().getTime() + 1000 * 60 * 2 - 5) / 1000.0));
            casper.echo("Received response from 2cpatcha :" + solvedjson.request);
            var captchaResponse = {responseToken: solvedjson.request, expiretime: expiredate};

            var newLinkFile;
            if (linkFile) {
                linkFile = linkFile.slice(0, -1);
                newLinkFile = linkFile + "," + JSON.stringify(captchaResponse) + "]";
                fs.write('captcharesponses.json', newLinkFile, 'w');
            } else {
                newLinkFile = "[" + JSON.stringify(captchaResponse) + "]";
                fs.write('captcharesponses.json', newLinkFile, 'w');
            }
        } else {
            casper.wait(5000, getGCAPTCHAToken, captchaId);
        }
    });
}


casper.then(function () {
    this.exit();
});

casper.run();





