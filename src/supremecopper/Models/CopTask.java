/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.List;
import javafx.scene.control.TextArea;
import javax.swing.SwingWorker;

/**
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class CopTask extends SwingWorker<Void, String>{
    private UserInfo userInfo;
    private TextArea outputArea;
    private UserPreferences prefs;
    private Process p;


    public CopTask(UserInfo userInfo, TextArea outputArea, UserPreferences prefs) throws FileNotFoundException {
        this.userInfo = userInfo;
        this.outputArea = outputArea;
        this.prefs = prefs;
        
//        //read response token from json file and check if currenttime is not before expire time
//        JsonParser parser = new JsonParser();
//        File file = new File("src/supremecopper/JavaScript/captcharesponses.json");
//        System.out.println(file.getAbsolutePath());
//        Object obj =  parser.parse(new FileReader(file));
//        JsonArray jsonArray = (JsonArray) obj;
//        for(JsonElement element: jsonArray){
//            JsonObject jsonObject = element.getAsJsonObject();
//            long expireTime  = jsonObject.get("expiretime").getAsLong();
//            long currentTime = System.currentTimeMillis()/1000;
//            
//            if(expireTime > currentTime){
//                System.out.println(jsonObject.get("responseToken").getAsString());
//                responseToken = jsonObject.get("responseToken").getAsString();
//                break;
//            }
//        }
    }
    
    @Override
    protected Void doInBackground() throws Exception {
        String cmdParameter = "--proxy=91.218.127.113:8080 "
        + "--keyWord=\\\""+prefs.getKeyword()+"\\\" "
        + "--size=\\\""+prefs.getSize()+"\\\" "
        + "--color=\\\""+prefs.getColor()+"\\\" "
        + "--fullName=\\\""+userInfo.getFullName()+"\\\" "
        + "--email="+userInfo.getEmail()+" "
        + "--tel=\\\""+userInfo.getTelephone()+"\\\" "
        + "--address=\\\""+userInfo.getAddress()+"\\\" "
        + "--city=\\\""+userInfo.getCity()+"\\\" "
        + "--zip=\\\""+userInfo.getZip()+"\\\" "
        + "--country=\\\""+userInfo.getCountry()+"\\\" "
        + "--type=\\\""+userInfo.getCreditcard().getType()+"\\\"  "
        + "--number=\\\""+userInfo.getCreditcard().getNumber()+"\\\"  "
        + "--month=\\\""+userInfo.getCreditcard().getMonth()+"\\\" "
        + "--year=\\\""+userInfo.getCreditcard().getYear()+"\\\"  "
        + "--cvv=\\\""+userInfo.getCreditcard().getCvv()+"\\\" "
        + "--category=\\\""+prefs.getCategory()+"\\\" "
        + "--refreshdelay=\\\""+prefs.getRefreshdelay()+"\\\" "
        + "--checkoutdelay=\\\""+prefs.getCheckoutdelay()+"\\\"";    
        
        String cmdLine = "cd \"src\\supremecopper\\JavaScript\" && casperjs supremecopper.js "+cmdParameter;
        ProcessBuilder builder = new ProcessBuilder(
            "cmd.exe", "/c", cmdLine);
        builder.redirectErrorStream(true);
        p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        Boolean stopped = false;
        while ((line = r.readLine()) != null && !stopped) {
            publish(line);
            if(p.isAlive() == false){
                stopped = true;
                publish("process stopped!");
            }
        }
        return null;
    }
    
    @Override
    protected void process(List<String> chuncks){
        for(String chunk : chuncks){
            outputArea.appendText(chunk+"\n");
        }
    } 
    
    public void stop(){
        p.destroy();
    } 
            
    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public TextArea getOutputArea() {
        return outputArea;
    }

    public void setOutputArea(TextArea outputArea) {
        this.outputArea = outputArea;
    }

    public UserPreferences getPrefs() {
        return prefs;
    }

    public void setPrefs(UserPreferences prefs) {
        this.prefs = prefs;
    }
}
    

