/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import javax.swing.SwingWorker;

/**
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class CaptchaTask extends SwingWorker<Void, String>{
    private Process p;
    private String siteKey;
    private String apiKey;

    public CaptchaTask(String siteKey, String apiKey) {
        this.siteKey = siteKey;
        this.apiKey = apiKey;
    }
           
    @Override
    protected Void doInBackground() throws Exception {
        String cmdParameter = "--proxy=91.218.127.113:8080 --sitekey=\\\""+siteKey+"\\\" --apikey=\\\""+apiKey+"\\\"";
        String cmdLine = "cd \"src\\supremecopper\\JavaScript\" && casperjs captchaSolver.js "+cmdParameter;
        ProcessBuilder builder = new ProcessBuilder(
            "cmd.exe", "/c", cmdLine);
        builder.redirectErrorStream(true);
        p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        Boolean stopped = false;
        while ((line = r.readLine()) != null && !stopped) {
            publish(line);
            if(p.isAlive() == false){
                stopped = true;
                publish("process stopped!");
            }
        }
        
        return null;
    }
    
    @Override
    protected void process(List<String> chuncks){
        for(String chunk : chuncks){
            System.out.println(chunk);
        }
    } 
    
    public void stop(){
        p.destroy();
    } 
}
