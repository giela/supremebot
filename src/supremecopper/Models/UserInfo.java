/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

import static supremecopper.Models.Tools.nullOrEmpty;

/**
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class UserInfo {
    private String fullName;
    private String email;
    private String telephone;
    private String address;
    private String city;
    private String zip;
    private String country;
    private Creditcard creditcard;
    

    public UserInfo() {
        
    }

    public UserInfo(String fullName, String email, String telephone, String address, String city, String zip, String country, Creditcard creditcard) {
        this.fullName = fullName;
        this.email = email;
        this.telephone = telephone;
        this.address = address;
        this.city = city;
        this.zip = zip;
        this.country = country;
        this.creditcard = creditcard;
    }
    public boolean isEmpty(){
        return (nullOrEmpty(fullName) || nullOrEmpty(email) || nullOrEmpty(telephone) || nullOrEmpty(address) || nullOrEmpty(city) || nullOrEmpty(zip) || nullOrEmpty(country));
    }
    
    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public String getCountry() {
        return country;
    }

    public Creditcard getCreditcard() {
        return creditcard;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCreditcard(Creditcard creditcard) {
        this.creditcard = creditcard;
    }
}
