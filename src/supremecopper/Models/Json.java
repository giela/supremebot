/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.Primitives;
import com.google.gson.stream.JsonReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import sun.misc.IOUtils;

/**
 *
 * @author giela
 */
public class Json {
    private static Gson gson = new Gson();

    public Json() {
    }
    /**
     * write an object to a json file
     * @param obj object to write   
     * @param path path of file to write to
     * @throws IOException 
     */
    public static void writeToJson(Object obj, String path) throws IOException{    
        try (FileWriter filewriter = new FileWriter(path)){
            filewriter.write(gson.toJson(obj));
            System.out.println("Successfully Copied JSON Object to "+path);
        }catch(FileNotFoundException fnf){
            File dir = new File(path);
            dir.mkdir();
            writeToJson(obj, path);
        }
    }
    
    public  static String readFromJson(String path) throws FileNotFoundException, IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            String everything = sb.toString();
            return everything;
        }
    }
}
