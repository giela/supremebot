/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

import static supremecopper.Models.Tools.nullOrEmpty;

/**
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class Creditcard {
    private String type;
    private String number;
    private String month;
    private int year;
    private String cvv;

    public Creditcard() {
    }

    public Creditcard(String type, String number, String month, int year, String cvv) {
        this.type = type;
        this.number = number;
        this.month = month;
        this.year = year;
        this.cvv = cvv;
    }
    
    public boolean isEmpty(){
        return(nullOrEmpty(type) || nullOrEmpty(number) || nullOrEmpty(month) || nullOrEmpty(cvv) || year < 2000);
    }
    
    public String getType() {
        return type;
    }

    public String getNumber() {
        return number;
    }
    
    public String getCensoredNumber() {
        return number.substring(number.length() -4);
    }
    public String getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public String getCvv() {
        return cvv;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
}
