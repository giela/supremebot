/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import supremecopper.SupremeCopper;

/**
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class Tools {
    public Stage  newPage(String resource, ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(resource));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("0टƐ-Y˩ԳƧ");
        stage.setScene(scene);
        stage.show();
        return stage;
    } 
    
    public void closePage(ActionEvent event){
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
    };
    
    public static boolean nullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }
    
    public void catchClosingWindow(Stage stage, Label userLabel, Label ccLabel, Button button){
        stage.setOnHiding((WindowEvent e) -> {
            button.setDisable(setTextLabel(userLabel, ccLabel));
        });
    };
    
    public boolean setTextLabel(Label userLabel, Label ccLabel){
        if(!SupremeCopper.userInfo.isEmpty()){
        userLabel.setText("Set up user: "+SupremeCopper.userInfo.getFullName());
            if(SupremeCopper.userInfo.getCreditcard() != null){
                ccLabel.setText("Set up creditcard: "+SupremeCopper.userInfo.getCreditcard().getCensoredNumber());
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }   
    }
    
    public static boolean isInteger(String s){
        return isInteger(s,10);
    }
    
    public static boolean isInteger(String s, int radix) {
    if(s.isEmpty()) return false;
    for(int i = 0; i < s.length(); i++) {
        if(i == 0 && s.charAt(i) == '-') {
            if(s.length() == 1) return false;
            else continue;
        }
        if(Character.digit(s.charAt(i),radix) < 0) return false;
    }
    return true;
}
}
