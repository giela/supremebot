/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

import java.time.LocalDateTime;


/**
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class CaptchaResponse {
    private String responseToken;
    private LocalDateTime expiretime;

    public CaptchaResponse(String responseToken, LocalDateTime expiretime) {
        this.responseToken = responseToken;
        this.expiretime = expiretime;
    }

    public String getResponseToken() {
        return responseToken;
    }

    public void setResponseToken(String responseToken) {
        this.responseToken = responseToken;
    }

    public LocalDateTime getExpiretime() {
        return expiretime;
    }

    public void setExpiretime(LocalDateTime expiretime) {
        this.expiretime = expiretime;
    }
    
    
}
