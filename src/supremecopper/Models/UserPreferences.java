/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

/**
 *
 * @author Naam:Giel Jurriëns, Klas:IS103, Studentnummer:500749216
 */
public class UserPreferences {
    private String color;
    private String size;
    private String keyword;
    private String category;
    private int checkoutdelay;
    private int refreshdelay;
    
    public UserPreferences() {
    }

    public UserPreferences(String color, String size, String keyword, String category, int checkoutdelay, int refreshdelay) {
        this.color = color;
        this.size = size;
        this.keyword = keyword;
        this.category = category;
        this.checkoutdelay = checkoutdelay;
        this.refreshdelay = refreshdelay;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getCheckoutdelay() {
        return checkoutdelay;
    }

    public void setCheckoutdelay(int checkoutdelay) {
        this.checkoutdelay = checkoutdelay;
    }

    public int getRefreshdelay() {
        return refreshdelay;
    }

    public void setRefreshdelay(int refreshdelay) {
        this.refreshdelay = refreshdelay;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    } 
}
