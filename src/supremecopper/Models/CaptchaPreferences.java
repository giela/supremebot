/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supremecopper.Models;

/**
 *
 * @author Naam:Giel Jurriëns Klas:IS103 studentnummer:500749216
 */
public class CaptchaPreferences {
    private String apiKey;
    private String siteKey;

    public CaptchaPreferences() {
    }
    
    
    public CaptchaPreferences(String apiKey, String siteKey) {
        this.apiKey = apiKey;
        this.siteKey = siteKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSiteKey() {
        return siteKey;
    }

    public void setSiteKey(String siteKey) {
        this.siteKey = siteKey;
    }
    
    
}
